-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.5798558299
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000021557 
   Number of Beta  Electrons                 16.0000021557 
   Total number of  Electrons                32.0000043114 
   Exchange energy                          -41.5697322216 
   Correlation energy                        -1.1715018925 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7412341141 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798558299 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.8223463982
     Dielectric Energy:     -0.0033121974
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0004053489
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.5798850722
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 16.0000026145 
   Number of Beta  Electrons                 16.0000026145 
   Total number of  Electrons                32.0000052291 
   Exchange energy                          -41.5694380396 
   Correlation energy                        -1.1714808035 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7409188431 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798850722 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.8567884458
     Dielectric Energy:     -0.0033462235
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0004053917
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.5798963711
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 16.0000029449 
   Number of Beta  Electrons                 16.0000029449 
   Total number of  Electrons                32.0000058897 
   Exchange energy                          -41.5689427876 
   Correlation energy                        -1.1714528594 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7403956470 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798963711 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.8954528464
     Dielectric Energy:     -0.0033715420
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0004054169
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.5798966914
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 16.0000029469 
   Number of Beta  Electrons                 16.0000029469 
   Total number of  Electrons                32.0000058937 
   Exchange energy                          -41.5688171891 
   Correlation energy                        -1.1714470557 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7402642448 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798966914 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.9046984471
     Dielectric Energy:     -0.0033715996
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0004054142
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 4
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.9572013105
        Electronic Contribution:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.763581
        Nuclear Contribution:
                  0    
      0       0.000000
      1      -0.000000
      2       0.006426
        Total Dipole moment:
                  0    
      0      -0.000000
      1      -0.000000
      2       0.770007
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.5798966924
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 16.0000029468 
   Number of Beta  Electrons                 16.0000029468 
   Total number of  Electrons                32.0000058937 
   Exchange energy                          -41.5688096191 
   Correlation energy                        -1.1714467156 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7402563347 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798966924 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.9046984471
     Dielectric Energy:     -0.0033717414
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0004054142
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 5
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         63.9619004136
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -548.5742649764
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0002632715
        Number of frequencies          :     9      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     494.668889
      7     1120.156404
      8     1279.864091
        Zero Point Energy (Hartree)    :          0.0065945877
        Inner Energy (Hartree)         :       -548.5645745745
        Enthalpy (Hartree)             :       -548.5636303654
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0096946985
        Vibrational entropy            :          0.0003609879
        Translational entropy          :          0.0096946985
        Entropy                        :          0.0282940215
        Gibbs Energy (Hartree)         :       -548.5919243869
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000   -0.000000000009    0.384002398208
               1 O      0.000000000000    1.259639961021   -0.373585199108
               2 O      0.000000000000   -1.259639961011   -0.373585199099
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000    0.000000000278    0.387022478787
               1 O      0.000000000000    1.257344697851   -0.375095239312
               2 O      0.000000000000   -1.257344698129   -0.375095239476
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000   -0.000000000353    0.389248414603
               1 O      0.000000000000    1.255859724486   -0.376208207411
               2 O      0.000000000000   -1.255859724133   -0.376208207192
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000   -0.000000000011    0.389265724066
               1 O      0.000000000000    1.255951900458   -0.376216862039
               2 O      0.000000000000   -1.255951900447   -0.376216862028
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000   -0.000000000011    0.389265724066
               1 O      0.000000000000    1.255951900458   -0.376216862039
               2 O      0.000000000000   -1.255951900447   -0.376216862028

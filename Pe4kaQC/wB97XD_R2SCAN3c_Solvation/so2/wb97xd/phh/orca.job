#!/bin/bash
# PBS Job
#PBS -V
#PBS -N hcalc.myproj.so2.wb97xd.phh.orca
#PBS -m ae
#PBS -q single
#PBS -l nodes=1:ppn=1
#PBS -l mem=8G
#PBS -o /tmp1/igor/orca.pbslogp
#PBS -e /tmp1/igor/orca.pbserrp
echo "Job is running on the compute node:"
echo "$PBS_NODEFILE"
echo ""
echo "The local scratch directory (located on the compute node) is:"
echo "/tmp1/igor/"
echo ""

export BASH_ENV=/usr/share/lmod/lmod/init/bash

# Initialize modules system
. /usr/share/lmod/lmod/init/bash >/dev/null

# Load baseline SUSE HPC environment
module try-add suse-hpc


TMPDIR=/tmp1/igor/$PBS_JOBID/
module use /home/abt-grimme/modulefiles/
module load orca/5.0.4
module load mkl/2022.1.0
export ODIR=/software/cluster/orca_5_0_4_linux_x86-64_openmpi411
LD_LIBRARY_PATH=/software/cluster/orca_5_0_4_linux_x86-64_openmpi411:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH
export PATH=$PATH:/software/openmpi-4.1.1a/bin/mpirun
export PATH=$PATH:/software/cluster/orca_5_0_4_linux_x86-64_openmpi411
module load openmpi/4.1.1a

cd /home/igor/calc/myproj/so2/wb97xd/phh
# Backup old out files if existing 
[ -e /home/igor/calc/myproj/so2/wb97xd/phh/orca.out_OLDER ] && rsync --bwlimit=1000 /home/igor/calc/myproj/so2/wb97xd/phh/orca.out /home/igor/calc/myproj/so2/wb97xd/phh/orca.out_OLDER
[ -e /home/igor/calc/myproj/so2/wb97xd/phh/orca.out_OLD ] && rsync --bwlimit=1000 /home/igor/calc/myproj/so2/wb97xd/phh/orca.out /home/igor/calc/myproj/so2/wb97xd/phh/orca.out_OLDER
[ -e /home/igor/calc/myproj/so2/wb97xd/phh/orca.out ] && rsync --bwlimit=1000 /home/igor/calc/myproj/so2/wb97xd/phh/orca.out /home/igor/calc/myproj/so2/wb97xd/phh/orca.out_OLD

# rsync --bwlimit=1000 everything to TMPDIR
mkdir $TMPDIR
rsync --bwlimit=1000 /home/igor/calc/myproj/so2/wb97xd/phh/orca.inp orca.gbw .xyz *.xyz orca.ges $TMPDIR
[ "0" != 0 ] && rsync --bwlimit=1000 0 $TMPDIR
[ "so2_input.xyz" != 0 ] && rsync --bwlimit=1000 so2_input.xyz $TMPDIR
cd $TMPDIR

export HOSTS_FILE="$PBS_NODEFILE"
cat $HOSTS_FILE>/home/igor/calc/myproj/so2/wb97xd/phh/hosts_file

echo "! Job ${PBS_JOBID} execution by suborca 6.0 !" > orca.out
echo "! Job started from ${PBS_O_HOST}, running  $(which orca) or ${ODIR}/orca on $(hostname) in /home/igor/calc/myproj/so2/wb97xd/phh" >> orca.out
echo "! Job uses /software/openmpi-4.1.1a/bin/mpirun for parallelization" >> orca.out

#sleep 500

#Execute locally but write output to startdir
$ODIR/orca orca.inp >> orca.out 

#rm tmpfiles after possible crash
rm *.tmp *.tmp0

# rsync --bwlimit=1000 things back
if [ "yes" = "yes" ] ; then
rsync --bwlimit=1000 orca* /home/igor/calc/myproj/so2/wb97xd/phh 
rsync --bwlimit=1000 hosts_file /home/igor/calc/myproj/so2/wb97xd/phh
fi

rm $TMPDIR -rf

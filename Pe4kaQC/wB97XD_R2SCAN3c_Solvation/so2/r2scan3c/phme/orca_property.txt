-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:     -548.5487516290
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 16.0000004601 
   Number of Beta  Electrons                 16.0000004601 
   Total number of  Electrons                32.0000009202 
   Exchange energy                          -41.3915009264 
   Correlation energy                        -1.1621971666 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.5536980930 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5487516290 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         303.5589469572
     Dielectric Energy:     -0.0043415382
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 1
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2137    16.0000     0.7863     3.2661     3.2661    -0.0000
  1   0     8.3932     8.0000    -0.3932     1.8884     1.8884    -0.0000
  2   0     8.3932     8.0000    -0.3932     1.8884     1.8884     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.633062
                0            16               2            8                1.633058
                1             8               2            8                0.255311
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0004018160
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:     -548.5724198245
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 15.9999999450 
   Number of Beta  Electrons                 15.9999999450 
   Total number of  Electrons                31.9999998901 
   Exchange energy                          -41.4812129888 
   Correlation energy                        -1.1671794745 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.6483924633 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5724198245 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         295.3709308642
     Dielectric Energy:     -0.0040243484
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0004041090
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:     -548.5772458366
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 16.0000000219 
   Number of Beta  Electrons                 16.0000000219 
   Total number of  Electrons                32.0000000438 
   Exchange energy                          -41.6190915166 
   Correlation energy                        -1.1736755837 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7927671003 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5772458366 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         285.6507710842
     Dielectric Energy:     -0.0034220885
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0004064122
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:     -548.5798863935
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 16.0000001736 
   Number of Beta  Electrons                 16.0000001736 
   Total number of  Electrons                32.0000003471 
   Exchange energy                          -41.5581977579 
   Correlation energy                        -1.1709654652 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7291632230 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5798863935 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         289.5755540788
     Dielectric Energy:     -0.0035926600
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0004053799
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:     -548.5799573725
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 16.0000001910 
   Number of Beta  Electrons                 16.0000001910 
   Total number of  Electrons                32.0000003819 
   Exchange energy                          -41.5666851106 
   Correlation energy                        -1.1713599001 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7380450106 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5799573725 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.9935520577
     Dielectric Energy:     -0.0035418475
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0004054992
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:     -548.5799548213
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 16.0000001932 
   Number of Beta  Electrons                 16.0000001932 
   Total number of  Electrons                32.0000003865 
   Exchange energy                          -41.5677560503 
   Correlation energy                        -1.1714101174 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7391661676 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5799548213 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.9205681346
     Dielectric Energy:     -0.0035312649
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0004055085
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 7
   prop. index: 1
        SCF Energy:     -548.5799547164
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 7
   prop. index: 1
   Number of Alpha Electrons                 16.0000001934 
   Number of Beta  Electrons                 16.0000001934 
   Total number of  Electrons                32.0000003869 
   Exchange energy                          -41.5677079765 
   Correlation energy                        -1.1714087412 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy              -42.7391167177 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)    -548.5799547164 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 7
   prop. index: 1
     Epsilon:                2.4000000000
     Refrac:                 1.4970000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                254
     Surface Area:         288.9230866790
     Dielectric Energy:     -0.0035301788
# -----------------------------------------------------------
$ Mayer_Pop
   description: The Mayer population analysis
   geom. index: 7
   prop. index: 1
     Number of atoms                     : 3
     Threshold for printing              : 0.1000000
     Number of bond orders printed       : 3
  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence
  ATOM       NA         ZA         QA         VA         BVA        FA
  0   0    15.2128    16.0000     0.7872     3.2592     3.2592    -0.0000
  1   0     8.3936     8.0000    -0.3936     1.7589     1.7589    -0.0000
  2   0     8.3936     8.0000    -0.3936     1.7589     1.7589     0.0000
      Bond orders larger than 0.1000000
           Atom A     A.N. of A          Atom B    A.N. of B              Bond order
                0            16               1            8                1.629583
                0            16               2            8                1.629572
                1             8               2            8                0.129340
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 7
   prop. index: 1
        Van der Waals Correction:       -0.0004055065
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 7
   prop. index: 1
       Filename                          : orca.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        1.9680613989
        Electronic Contribution:
                  0    
      0       0.000000
      1       0.000022
      2       0.796679
        Nuclear Contribution:
                  0    
      0       0.000000
      1       0.000000
      2      -0.022400
        Total Dipole moment:
                  0    
      0       0.000000
      1       0.000022
      2       0.774279
# -----------------------------------------------------------
$ Hessian
   description: Details about the Hessian
   geom. index: 7
   prop. index: 1
Normal modes:
Number of Rows: 9 Number of Columns: 9
                  0          1          2          3          4          5    
      0       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      1       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      2       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      3       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      4       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      5       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      6       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      7       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
      8       0.000000   0.000000   0.000000   0.000000   0.000000   0.000000
                  6          7          8    
      0      -0.000000   0.000000  -0.000000
      1       0.000000   0.000001   0.516179
      2       0.397202   0.357151  -0.000001
      3       0.000000   0.000000  -0.000000
      4      -0.512577   0.555130  -0.517180
      5      -0.397971  -0.357842   0.315128
      6      -0.000000  -0.000000   0.000000
      7       0.512576  -0.555131  -0.517179
      8      -0.397971  -0.357843  -0.315127
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 7
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :         64.0580000000
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :       -548.5743227388
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0002636587
        Number of frequencies          :     9      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6     494.817514
      7     1117.384183
      8     1274.937376
        Zero Point Energy (Hartree)    :          0.0065773868
        Inner Energy (Hartree)         :       -548.5646491505
        Enthalpy (Hartree)             :       -548.5637049414
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0096959990
        Vibrational entropy            :          0.0003614111
        Translational entropy          :          0.0096959990
        Entropy                        :          0.0282978715
        Gibbs Energy (Hartree)         :       -548.5920028129
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     1 
    Coordinates:
               0 S      0.000000000000   -0.000000070661    0.464422083483
               1 O      0.000000000000    1.351921804805   -0.413795062960
               2 O      0.000000000000   -1.351921734143   -0.413795020523
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     2 
    Coordinates:
               0 S      0.000000000000   -0.000000215937    0.431689984093
               1 O      0.000000000000    1.292090208502   -0.397429060795
               2 O      0.000000000000   -1.292089992564   -0.397428923298
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     3 
    Coordinates:
               0 S      0.000000000000    0.000000451168    0.380129336378
               1 O      0.000000000000    1.225427651215   -0.371648525816
               2 O      0.000000000000   -1.225428102383   -0.371648810561
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     4 
    Coordinates:
               0 S      0.000000000000    0.000000051613    0.394452985642
               1 O      0.000000000000    1.259239188467   -0.378810473035
               2 O      0.000000000000   -1.259239240081   -0.378810512607
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     5 
    Coordinates:
               0 S      0.000000000000    0.000000092123    0.390176742303
               1 O      0.000000000000    1.256198251380   -0.376672338844
               2 O      0.000000000000   -1.256198343504   -0.376672403459
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     6 
    Coordinates:
               0 S      0.000000000000   -0.000000132556    0.389296675995
               1 O      0.000000000000    1.256109102096   -0.376232374222
               2 O      0.000000000000   -1.256108969540   -0.376232301773
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    3 
    Geometry Index:     7 
    Coordinates:
               0 S      0.000000000000   -0.000000098125    0.389227663692
               1 O      0.000000000000    1.256197605044   -0.376197857587
               2 O      0.000000000000   -1.256197506919   -0.376197806106

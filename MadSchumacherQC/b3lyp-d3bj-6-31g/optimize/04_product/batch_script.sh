#!/bin/bash


#SBATCH --partition=allcpu,maxcpu   # this is the default partition.
#SBATCH --time=5:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like 
#SBATCH --job-name  QuantumChemistryRocks!
#SBATCH --output     mol.inp.out  # File to which STDOUT will be written
#SBATCH --error    mol.inp.err  # File to which STDERR will be written
#SBATCH --mail-type END                 # Type of email notification- BEGIN,END,FAIL,ALL



export LD_PRELOAD=""
source /etc/profile.d/modules.sh
module load maxwell orca
$(which orca)   mol.inp

# now, clean
rm *.ges *tmp* *.densities *.gbw *loc* *.opt *propert*.txt



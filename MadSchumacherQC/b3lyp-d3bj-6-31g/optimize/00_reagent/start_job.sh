#!/bin/bash
inpf=$(ls | grep '\.inp$')
homedir=$(pwd)

echo '''#!/bin/bash


#SBATCH --partition=allcpu,maxcpu   # this is the default partition.
#SBATCH --time=5:00:00        # default is 1h. The maximum is partition dependent, have a look at sview or scontrol for details.
#SBATCH --nodes=1              # Number of nodes. If your job can consume variable number of nodes you might want to use something like ''' > batch_script.sh

#echo "#SBATCH --workdir  " $homedir >>  batch_script.sh
echo "#SBATCH --job-name  QuantumChemistryRocks!" >> batch_script.sh
echo  "#SBATCH --output    " $inpf".out  # File to which STDOUT will be written" >> batch_script.sh
echo  "#SBATCH --error   "  $inpf".err  # File to which STDERR will be written" >> batch_script.sh
echo '''#SBATCH --mail-type END                 # Type of email notification- BEGIN,END,FAIL,ALL

''' >> batch_script.sh

echo '''
export LD_PRELOAD=""
source /etc/profile.d/modules.sh
module load maxwell orca''' >> batch_script.sh

echo '$(which orca)  ' $inpf >> batch_script.sh

echo '''
# now, clean
rm */*.ges */*tmp* */*.densities */*.gbw */*loc*

''' >> batch_script.sh

sbatch batch_script.sh

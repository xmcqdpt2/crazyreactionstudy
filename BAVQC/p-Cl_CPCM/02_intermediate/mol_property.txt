-------------------------------------------------------------
----------------------- !PROPERTIES! ------------------------
-------------------------------------------------------------
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 1
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 1
   prop. index: 1
        SCF Energy:    -1407.7697313618
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 1
   prop. index: 1
   Number of Alpha Electrons                 54.9999750432 
   Number of Beta  Electrons                 54.9999750432 
   Total number of  Electrons               109.9999500864 
   Exchange energy                         -121.9068658418 
   Correlation energy                        -3.8743050751 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7811709169 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697313618 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 1
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                969
     Surface Area:         775.2144153428
     Dielectric Energy:     -0.0056070726
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 1
   prop. index: 1
        Van der Waals Correction:       -0.0075437768
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 2
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 2
   prop. index: 1
        SCF Energy:    -1407.7697782062
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 2
   prop. index: 1
   Number of Alpha Electrons                 54.9999746727 
   Number of Beta  Electrons                 54.9999746727 
   Total number of  Electrons               109.9999493454 
   Exchange energy                         -121.9037671503 
   Correlation energy                        -3.8740811467 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7778482970 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697782062 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 2
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                972
     Surface Area:         775.5825354603
     Dielectric Energy:     -0.0056687237
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 2
   prop. index: 1
        Van der Waals Correction:       -0.0075426616
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 3
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 3
   prop. index: 1
        SCF Energy:    -1407.7697940759
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 3
   prop. index: 1
   Number of Alpha Electrons                 54.9999745487 
   Number of Beta  Electrons                 54.9999745487 
   Total number of  Electrons               109.9999490973 
   Exchange energy                         -121.9023932190 
   Correlation energy                        -3.8739796810 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7763729000 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697940759 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 3
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                973
     Surface Area:         775.7100774532
     Dielectric Energy:     -0.0057095801
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 3
   prop. index: 1
        Van der Waals Correction:       -0.0075429925
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 4
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 4
   prop. index: 1
        SCF Energy:    -1407.7697973800
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 4
   prop. index: 1
   Number of Alpha Electrons                 54.9999745525 
   Number of Beta  Electrons                 54.9999745525 
   Total number of  Electrons               109.9999491050 
   Exchange energy                         -121.9025489160 
   Correlation energy                        -3.8739854379 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7765343539 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697973800 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 4
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                972
     Surface Area:         775.6704414590
     Dielectric Energy:     -0.0057183462
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 4
   prop. index: 1
        Van der Waals Correction:       -0.0075436223
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 5
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 5
   prop. index: 1
        SCF Energy:    -1407.7697980675
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 5
   prop. index: 1
   Number of Alpha Electrons                 54.9999746221 
   Number of Beta  Electrons                 54.9999746221 
   Total number of  Electrons               109.9999492443 
   Exchange energy                         -121.9027801121 
   Correlation energy                        -3.8739994718 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7767795839 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697980675 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 5
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                972
     Surface Area:         775.6103894170
     Dielectric Energy:     -0.0057173556
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 5
   prop. index: 1
        Van der Waals Correction:       -0.0075438680
# -----------------------------------------------------------
$ SCF_Electric_Properties
   description: The SCF Calculated Electric Properties
   geom. index: 5
   prop. index: 1
       Filename                          : mol.scfp 
       Do Dipole Moment Calculation      : true 
       Do Quadrupole Moment Calculation  : false 
       Do Polarizability Calculation     : false 
** Dipole moment part of electric properties **
        Magnitude of dipole moment (Debye) :        2.5265480305
        Electronic Contribution:
                  0    
      0       1.503381
      1       0.322910
      2       0.565694
        Nuclear Contribution:
                  0    
      0      -1.719099
      1      -0.686655
      2       0.333857
        Total Dipole moment:
                  0    
      0      -0.215717
      1      -0.363745
      2       0.899551
# -----------------------------------------------------------
$ PAL_Flags
   description: The PAL Flags
   geom. index: 6
   prop. index: 1
        Diskflag:     2
# -----------------------------------------------------------
$ SCF_Energy
   description: The SCF energy
   geom. index: 6
   prop. index: 1
        SCF Energy:    -1407.7697980688
# -----------------------------------------------------------
$ DFT_Energy
   description: The DFT energy
   geom. index: 6
   prop. index: 1
   Number of Alpha Electrons                 54.9999746220 
   Number of Beta  Electrons                 54.9999746220 
   Total number of  Electrons               109.9999492440 
   Exchange energy                         -121.9027810824 
   Correlation energy                        -3.8739993780 
   Correlation energy NL                      0.0000000000 
   Exchange-Correlation energy             -125.7767804605 
   Embedding correction                       0.0000000000 
   Total DFT Energy (No VdW correction)   -1407.7697980688 
# -----------------------------------------------------------
$ Solvation_Details
   description: Details concerning solvation
   geom. index: 6
   prop. index: 1
     Epsilon:                2.2800000000
     Refrac:                 1.5010000000
     RSolv:                  1.3000000000
     Surface Type:                      2
     Number of Points:                972
     Surface Area:         775.6103894170
     Dielectric Energy:     -0.0057174650
# -----------------------------------------------------------
$ VdW_Correction
   description: The Van der Waals Correction
   geom. index: 6
   prop. index: 1
        Van der Waals Correction:       -0.0075438680
# -----------------------------------------------------------
$ THERMOCHEMISTRY_Energies
   description: The Thermochemistry energies
   geom. index: 6
   prop. index: 1
        Temperature (Kelvin)           :        298.1500000000
        Pressure (atm)                 :          1.0000000000
        Total Mass (AMU)               :        216.9600418485
        Spin Degeneracy                :          1.0000000000
        Electronic Energy (Hartree)    :      -1407.7570918940
        Translational Energy (Hartree) :          0.0014162714
        Rotational Energy (Hartree)    :          0.0014162714
        Vibrational Energy (Hartree)   :          0.0082128371
        Number of frequencies          :     51      
        Scaling Factor for frequencies :          1.0000000000
        Vibrational frequencies        :     
                  0    
      0       0.000000
      1       0.000000
      2       0.000000
      3       0.000000
      4       0.000000
      5       0.000000
      6      31.491053
      7      54.705399
      8     101.843000
      9     128.150754
     10     202.892997
     11     253.018479
     12     280.187040
     13     299.150606
     14     332.124216
     15     406.227449
     16     407.295478
     17     436.204090
     18     476.710244
     19     492.333661
     20     514.472821
     21     642.150118
     22     647.600410
     23     650.840657
     24     675.817840
     25     710.882768
     26     754.307590
     27     829.872251
     28     836.424001
     29     892.687124
     30     932.901313
     31     957.820949
     32     974.787914
     33     1034.315701
     34     1070.401110
     35     1097.577834
     36     1136.884818
     37     1203.850768
     38     1215.005177
     39     1321.416504
     40     1332.576899
     41     1346.670810
     42     1431.360807
     43     1515.475768
     44     1590.035425
     45     1623.188050
     46     1626.527935
     47     3194.419581
     48     3197.243965
     49     3206.493801
     50     3209.392873
        Zero Point Energy (Hartree)    :          0.1031457208
        Inner Energy (Hartree)         :      -1407.6429007932
        Enthalpy (Hartree)             :      -1407.6419565842
        Electronic entropy             :          0.0000000000
        Rotational entropy             :          0.0152480289
        Vibrational entropy            :          0.0141060108
        Translational entropy          :          0.0152480289
        Entropy                        :          0.0493222450
        Gibbs Energy (Hartree)         :      -1407.6912788292
        Is Linear                      :                 false
# -------------------------------------------------------------
----------------------- !GEOMETRIES! ------------------------
# -------------------------------------------------------------
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     1 
    Coordinates:
               0 C      0.091692918706    0.110633364136   -0.099621559614
               1 C      1.083244314628    1.066758145651   -0.233985433569
               2 C      0.410551477661   -1.128249015300    0.453099105452
               3 C      2.393589147199    0.787340357983    0.176793494318
               4 C      2.694925856818   -0.461354872995    0.728523974454
               5 C      1.703264396836   -1.422637970361    0.868952166334
               6 H      0.849425549070    2.037068757731   -0.659205601070
               7 C      3.432836487607    1.799023192576    0.032271678035
               8 H     -0.924035678967    0.319803864904   -0.416624431348
               9 Cl    -0.842087414271   -2.333642291797    0.627604946813
              10 H      3.707663455162   -0.684766587380    1.045234503416
              11 H      1.931485943262   -2.393132325517    1.295334707616
              12 N      3.271981747153    2.919126905046   -0.575958087195
              13 O      4.459745953492    3.662234075520   -0.499483778325
              14 O      4.657053439220    1.535698107368    0.588856124498
              15 S      5.804320424890    2.682245519286   -0.009734694611
              16 O      6.430211981535    2.097880773149   -1.198027115203
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     2 
    Coordinates:
               0 C      0.090193381541    0.110515499006   -0.099365422033
               1 C      1.081022147906    1.067364535222   -0.234298318842
               2 C      0.411691462482   -1.127977704943    0.453099074350
               3 C      2.392312722617    0.788093646535    0.175467209569
               4 C      2.695577676377   -0.460604735988    0.727315001599
               5 C      1.704723191475   -1.422577836192    0.868570567955
               6 H      0.844047148575    2.037499297793   -0.658361714673
               7 C      3.431908363350    1.799813413361    0.030601493304
               8 H     -0.926097907437    0.319746833121   -0.415468719460
               9 Cl    -0.840933013060   -2.335144361854    0.628558774500
              10 H      3.708434760886   -0.684392756186    1.043917942754
              11 H      1.934618263195   -2.392875231471    1.295196375096
              12 N      3.274424425148    2.919739294070   -0.579605663972
              13 O      4.464285242082    3.662414342252   -0.499517439703
              14 O      4.655201644561    1.536665772930    0.588287960715
              15 S      5.806330415160    2.681302210660   -0.004927770394
              16 O      6.428130075141    2.094447781683   -1.195439350766
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     3 
    Coordinates:
               0 C      0.089222199149    0.110763098603   -0.098437004130
               1 C      1.079789093408    1.068087105587   -0.234010373151
               2 C      0.412885119679   -1.127319719243    0.453355587810
               3 C      2.391571794863    0.788598340568    0.174229189676
               4 C      2.696426612957   -0.460120270019    0.725599649328
               5 C      1.705998645480   -1.422589397701    0.867845700976
               6 H      0.839945307371    2.037966070058   -0.657083644344
               7 C      3.431252664349    1.800100665854    0.028593859635
               8 H     -0.927274623323    0.320446207005   -0.413455526150
               9 Cl    -0.840015441829   -2.335856639205    0.629715074969
              10 H      3.709282529808   -0.684772153307    1.041468496542
              11 H      1.937361796921   -2.392612094913    1.294235021045
              12 N      3.275722564421    2.919558071129   -0.583871442040
              13 O      4.466754313991    3.662352541684   -0.501975471341
              14 O      4.652946631912    1.537313385157    0.588820441367
              15 S      5.805591200319    2.680608728507   -0.000460004263
              16 O      6.428409590526    2.091506060234   -1.190539555930
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     4 
    Coordinates:
               0 C      0.089159098630    0.110889103816   -0.098053913545
               1 C      1.079779528381    1.068408571150   -0.233632323719
               2 C      0.413407820668   -1.127057347110    0.453267312164
               3 C      2.391526904366    0.788915003451    0.174142444099
               4 C      2.696772734259   -0.459855691486    0.725055261690
               5 C      1.706428082410   -1.422593379130    0.867372815196
               6 H      0.838948200474    2.038163446173   -0.656392869840
               7 C      3.431095860597    1.800493298007    0.028331307646
               8 H     -0.927299760143    0.320877613916   -0.412779404608
               9 Cl    -0.839590187890   -2.336049099335    0.629683692530
              10 H      3.709576684366   -0.684983394190    1.040627085788
              11 H      1.938323553152   -2.392556478452    1.293458827158
              12 N      3.276005085576    2.919022424341   -0.585894895158
              13 O      4.467015266465    3.662113480979   -0.502934625561
              14 O      4.652085234440    1.538643815421    0.590537937156
              15 S      5.804896739394    2.680223927895    0.000383523567
              16 O      6.427739154853    2.089374704555   -1.189142174562
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     5 
    Coordinates:
               0 C      0.089028136033    0.111007219078   -0.097501861478
               1 C      1.079758415966    1.068604357606   -0.233121755301
               2 C      0.413557773884   -1.126957375189    0.453304274850
               3 C      2.391535899216    0.789005495004    0.173972104539
               4 C      2.696962155697   -0.459882637039    0.724343473615
               5 C      1.706614590239   -1.422731367190    0.866811951336
               6 H      0.838316251959    2.038355760737   -0.655518404042
               7 C      3.430983947054    1.800708407941    0.027955403386
               8 H     -0.927455752941    0.321376658084   -0.411801406418
               9 Cl    -0.839509655744   -2.336073264410    0.629983686766
              10 H      3.709790729128   -0.685428987094    1.039481592721
              11 H      1.938895770037   -2.392729570268    1.292541496324
              12 N      3.276137588977    2.918215900269   -0.587962784056
              13 O      4.466923927167    3.661739635653   -0.504681685706
              14 O      4.651526797574    1.540043384697    0.592025891284
              15 S      5.804200137094    2.680572101941    0.001107753776
              16 O      6.428603288660    2.088204280180   -1.186909731597
------------------------ !GEOMETRY! -------------------------
    Number of atoms:    17 
    Geometry Index:     6 
    Coordinates:
               0 C      0.089028136033    0.111007219078   -0.097501861478
               1 C      1.079758415966    1.068604357606   -0.233121755301
               2 C      0.413557773884   -1.126957375189    0.453304274850
               3 C      2.391535899216    0.789005495004    0.173972104539
               4 C      2.696962155697   -0.459882637039    0.724343473615
               5 C      1.706614590239   -1.422731367190    0.866811951336
               6 H      0.838316251959    2.038355760737   -0.655518404042
               7 C      3.430983947054    1.800708407941    0.027955403386
               8 H     -0.927455752941    0.321376658084   -0.411801406418
               9 Cl    -0.839509655744   -2.336073264410    0.629983686766
              10 H      3.709790729128   -0.685428987094    1.039481592721
              11 H      1.938895770037   -2.392729570268    1.292541496324
              12 N      3.276137588977    2.918215900269   -0.587962784056
              13 O      4.466923927167    3.661739635653   -0.504681685706
              14 O      4.651526797574    1.540043384697    0.592025891284
              15 S      5.804200137094    2.680572101941    0.001107753776
              16 O      6.428603288660    2.088204280180   -1.186909731597

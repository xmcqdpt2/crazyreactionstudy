#!/bin/bash
crdr=$(pwd)

cd ${crdr}/00_reagent/energy
$HOME/orca/orca inp > out
echo "00 done"
cd ${crdr}/01_pre-intermediate/energy
$HOME/orca/orca inp > out
echo "01 done"
cd ${crdr}/02_intermediate/energy
$HOME/orca/orca inp > out
echo "02 done"
cd ${crdr}/03_post-intermediate/energy
$HOME/orca/orca inp > out
echo "03 done"
cd ${crdr}/04_product/energy
$HOME/orca/orca inp > out
echo "04 done"
cd ${crdr}/just_so2/energy
$HOME/orca/orca inp > out
echo "so2 done"
